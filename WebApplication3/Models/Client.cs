﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class Client
    {
        public int Id { get; set; }
        [Required]
        [DisplayName("ФИО")]
        public string Full_Name { get; set; }
        [DisplayName("Дата рождения")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0: dd/MM/yyyy} ")]
        public DateTime Birthday { get; set; }
        [DisplayName("Пол")]
        public Gender Gender { get; set; }
        [Required]
        [DisplayName("Адрес проживания")]
        public string Address { get; set; }
        [DisplayName("Пол")]
        public int GenderId { get; set; }
    }

}
