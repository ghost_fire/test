﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class Contract
    {
        public int ContractId { get; set; }
        [Required]
        [DisplayName("Номер договора")]
        public string Number { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0: dd/MM/yyyy} ")]
        [DisplayName("Дата начала действия договора")]
        public DateTime Date_Begin { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0: dd/MM/yyyy} ")]
        [DisplayName("Дата окончания действия договора")]
        public DateTime Date_End { get; set; }


        [DisplayName("Клиенты")]
        public int ClientId { get; set; }
        [DisplayName("Клиент")]
        public Client Client { get; set; }
    }
}
