﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class Gender
    {
        public int Id { get; set; }
        public string GenderType { get; set; }
    }
}
