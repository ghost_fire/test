﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class TestContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Gender> Genders { get; set; }

        public TestContext(DbContextOptions<TestContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
