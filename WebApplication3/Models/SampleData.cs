﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public static class SampleData
    {
        public static void Initialize(TestContext context)
        {
            if (!context.Genders.Any())
            {
                context.Genders.AddRange(
                    new Gender
                    {
                        GenderType = "мужской"
                    },
                    new Gender
                    {
                        GenderType = "женский"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
